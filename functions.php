<?php

/**
 *  Gets a new mysql connection
 */
function Connection()
{
    $conn = new mysqli('localhost', 'root', 'root', 'workshop05');
    if ($conn->connect_errno) {
        printf("Connect failed: %s\n", $conn->connect_error);
        die;
    }
    return $conn;
}
